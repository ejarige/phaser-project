Projet Phaser
=========
 
Ce projet de 1 semaine réalisé durant mes études s'inspire de la série de jeux de rôle Persona.

Entièrement codé en Javascript avec la librairie Phaser.io, le jeu est conçu selon une pseudo-architecture MVC séparant au mieux le Modèle (bases de données en JSON pour les personnages, ennemis, séquences d'attaque, etc.) , la Vue (sprites et animations) et le Controleur (système de combat).

Le jeu contient un mécanisme de combat au tour par tour complet avec statistiques (attaque, défense, vitesse), faiblesses et résistances, système de garde et attaques spéciales. Il est composé de 7 niveaux de difficulté croissante, dont 2 boss et un boss final. Chaque boss possède un *pattern* scripté et doit être vaincu avec une stratégie particulière. Il y a 4 personnages jouables et 7 types d'ennemis différents. On utilise les cookies pour faire des sauvegardes automatiques.

Les ressources (ressources graphiques, musiques, voix, personnages...) proviennent des différents jeux de la saga.

Voici un let's play du jeu complet sur YouTube :

[![P3:Wiping All Out](https://img.youtube.com/vi/npDzFGQJhKk/0.jpg)](https://www.youtube.com/watch?v=npDzFGQJhKk)

eric.jarrige@live.fr - [Voir mon Portfolio](https://erjarrige.wixsite.com/portfolio)
