function Enemy(params) {
    this.params = params;
    this.x = this.y = 0;
    this.scale = this.params.scale ? this.params.scale : .5;
    this.offsetY = this.params.offsetY ? this.params.offsetY : 0;
    this.hp = params.hp;
    this.speed = params.speed;
    this.type = params.type;
    this.strength = params.strength;
    this.affinity = params.affinity;
    this.alive = true;

    // prepare sprite
    this.sprite = game.add.sprite(this.x, this.y, this.params.sprite);
    this.sprite.anchor.setTo(.5,1);

    this.target = this.sprite.addChild(game.make.sprite(0, -this.sprite.height/3, 'target'));
    this.target.anchor.setTo(.5,.5);
    this.target.scale.setTo(1/this.scale,1/this.scale);
    this.target.alpha = 0;

    this.sprite.scale.setTo(this.scale, this.scale);

    // prepare tweens
    this.tweens = {
        idle    : game.add.tween(this.sprite.scale).to({x:this.scale+this.scale*6/100,y:this.scale+this.scale*6/100}, 1000, Phaser.Easing.Default, true, 0, -1, true),
        idle2   : game.add.tween(this.sprite).to({angle: this.scale < 1 ? 2 : 1}, 1000, Phaser.Easing.Default, true, 0, -1, true),
        target  : game.add.tween(this.target).to({angle: 360}, 6000, Phaser.Easing.Default, true, 0, -1),
        attack  : game.add.tween(this.sprite.scale).to({x: this.scale+.2,y:this.scale+.2}, 100, "Linear", false, 0, 2, true),
        die     : game.add.tween(this.sprite).to({alpha: 0}, 2000, "Linear"),
        //hurt : game.add.tween(this.sprite).to({x: '+20', angle: 20}, 500, Phaser.Easing.Default, true, 0, -1, true)
    };

    this.setXY = function(x,y){
        this.x = this.sprite.x = x;
        this.y = this.sprite.y = y;
    };

    this.getXY = function(){
        return {x: this.x, y: this.y+this.offsetY};
    };

    this.getName = function(){
        return this.params.name;
    };

    this.getInfo = function(){
        return this.params.name+'\t\t'+'HP : '+this.hp+' / '+this.params.hp;
    };

    this.isAlive = function(){
        return this.alive;
    };

    this.isFlying = function(){
        return this.offsetY;
    };

    this.toggleSelect = function(bool){
        this.target.alpha = +bool;
    };

    this.freeze = function(bool){
        if(bool){
            for(var t in this.tweens)
                this.tweens[t].pause();
        } else {
            for(var t in this.tweens)
                this.tweens[t].resume();
        }
    };

    this.attack = function(targets, target, nTurn, cb){
        /*this.tweens.attack.onLoop.addOnce(function(){
            cb();
        });*/ // bug... temporary fix below
        setTimeout(function(){
            cb();
        }, 2000);

        this.tweens.attack.start();

        var dmg = Math.floor(this.strength + this.strength*game.rnd.integerInRange(0, 20)/100 - this.strength/10);

        var element;
        if(this.type == 'boss_biker'){
            // weak type each actor each turn
            element = nTurn%2 ? 'thunder' : 'fire';
            target  = nTurn%2 && targets[nTurn%2].isAlive() ? 1 : 0;

        } else if(this.type == 'boss_anubis'){
            // ice until 90%hp, fire until 70%hp etc + always target weak
            var percentHp = 100*this.hp/this.params.hp;
            if(percentHp > 80){
                element = 'ice';
            } else if(percentHp > 60){
                element = 'fire';
            } else if(percentHp > 40){
                element = 'thunder';
            } else if(percentHp > 20){
                element = 'wind';
            } else if(percentHp > 0){
                if(nTurn%4 == 0){
                    element = 'ice';
                } else if(nTurn%4 == 2) {
                    element = 'fire';
                } else if(nTurn%4 == 3) {
                    element = 'thunder';
                } else {
                    element = 'wind';
                }
            }

            for(var t in targets){
                if(~targets[t].affinity.weak.indexOf(element) && targets[t].isAlive()){
                    target = t;
                }
            }

        } else {
            element = this.type;
        }
        targets[target].removeHp(dmg, element);
    };

    this.removeHp = function(val, element, animDelay){
        this.freeze(true);
        this.sprite.anchor.setTo(.5,.5);

        game.add.tween(this.sprite).to({y:this.y-20}, 100, Phaser.Easing.Default, true, animDelay+200, 0, true);

        var down = game.add.tween(this.sprite).to({angle: 70}, 100, Phaser.Easing.Default, true, animDelay);
        down.onComplete.addOnce(function(that){
            return function(){
                var up = game.add.tween(that.sprite).to({angle: 0}, 1000, Phaser.Easing.Default, true);
                up.onComplete.addOnce(function(that){
                    return function(){
                        that.freeze(false);
                    }
                }(that));
            }
        }(this));

        this.sprite.anchor.setTo(.5,1);

        //game.add.tween(this.sprite).to({x: this.x, angle: 0}, 100, Phaser.Easing.Default, true, 2500);

        // check type affinity
        if(~this.affinity.weak.indexOf(element)){
            val *= 3;
        } else if(~this.affinity.strong.indexOf(element)){
            val /= 3;
        } else if(~this.affinity.block.indexOf(element)){
            val = 0;
        }

        // float to int for better display
        val = Math.floor(val);
        this.hp -= val;

        // damage pop up
        var colorByType = '#fff';
        switch (element){ // version future : faire un json des types ! ;)
            case 'ice'      : colorByType = '#0000ff'; break;
            case 'wind'     : colorByType = '#00ff00'; break;
            case 'fire'     : colorByType = '#ff0000'; break;
            case 'thunder'  : colorByType = '#ffff00'; break;
        }

        var dmgText = game.add.text(this.sprite.x,this.sprite.y-this.sprite.height/2,val ? '-'+val : 'BLOCK',{font: '50px Arial', fill: colorByType});
        dmgText.anchor.setTo(.5,.5);
        dmgText.alpha = 0;
        dmgText.stroke = "#000000";
        dmgText.strokeThickness = 2;

        // care actor animation delay
        var dmgTween1 = game.add.tween(dmgText).to({alpha:1}, 300, "Linear", false, animDelay);
        var dmgTween2 = game.add.tween(dmgText).to({y:'-100', alpha:0}, 1000, "Linear");

        dmgTween2.onComplete.addOnce(function(){
            dmgText.destroy();
        });
        dmgTween1.onComplete.addOnce(function(){
            dmgTween2.start();
        });

        dmgTween1.start();


        if(this.hp <= 0){
            this.alive = false;
            this.die();
        }
    };

    this.die = function(){
        this.tweens.die.start();
    }
}