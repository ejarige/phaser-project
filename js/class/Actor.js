function Actor(name, order){
    this.actorsPos = [
        {x:350,y:525},
        {x:275,y:500},
        {x:200,y:525},
        {x:275,y:550}
    ];

    this.id   = name;

    var actor = game.cache.getJSON('JSON_actors')[this.id];

    this.name   = actor.name;
    this.order  = order;
    this.ex     = 0;
    this.speed  = actor.speed;
    this.maxHp  = this.hp = actor.hp;
    this.type   = actor.type;
    this.affinity = actor.affinity;
    this.strength  = actor.strength;
    this.guard = false;
    this.alive = true;

    this.x = -this.actorsPos[order].x;
    this.y = this.actorsPos[order].y;

    this.portraitX = 70+this.order*80;
    this.portraitY = 50+(this.order%2)*40;

    // prepare sprite
    this.sprite = game.add.sprite(this.x,this.y, this.id+'_sprite');
    this.sprite.anchor.setTo(.5,1);
    this.sprite.scale.setTo(.5,.5);
    this.sprite.scale.x *= -1;

    // prepare cutIn
    this.cutIn = game.add.sprite(-50, -100, this.id+'_cut_in');
    this.cutIn.alpha = 0;

    // prepare special background
   /* var fragmentSrc = [
        "precision mediump float;",
        "uniform vec2      resolution;",
        "uniform float     time;",

        "void main( void )",
        "{",
        "vec2 p = ( gl_FragCoord.xy / resolution.xy ) * 2.0 - 1.0;",

        "vec3 c = vec3( 0.0 );",

        "float amplitude = 0.50;",
        "float glowT = sin(time) * 0.5 + 0.5;",
        "float glowFactor = mix( 0.15, 0.35, glowT );",

        "c += vec3(0.02, 0.03, 0.13) * ( glowFactor * abs( 1.0 / sin(p.x + sin( p.y + time ) * amplitude ) ));",
        "c += vec3(0.02, 0.10, 0.03) * ( glowFactor * abs( 1.0 / sin(p.x + cos( p.y + time+1.00 ) * amplitude+0.1 ) ));",
        "c += vec3(0.15, 0.05, 0.20) * ( glowFactor * abs( 1.0 / sin(p.y + sin( p.x + time+1.30 ) * amplitude+0.15 ) ));",
        "c += vec3(0.20, 0.05, 0.05) * ( glowFactor * abs( 1.0 / sin(p.y + cos( p.x + time+3.00 ) * amplitude+0.3 ) ));",
        "c += vec3(0.17, 0.17, 0.05) * ( glowFactor * abs( 1.0 / sin(p.y + cos( p.x + time+5.00 ) * amplitude+0.2 ) ));",

        "gl_FragColor = vec4( c, 1.0 );",
        "}"
    ];

    var specialFilter = new Phaser.Filter(game, null, fragmentSrc);
    specialFilter.setResolution(800, 600);

    this.specialBg = game.add.sprite();
    this.specialBg.width = 800;
    this.specialBg.height = 600;
    this.specialBg.alpha = 0;

    this.specialBg.filters = [ specialFilter ];*/

    // prepare voice
    this.voice = game.add.audioSprite(this.id+'_voice');
    this.voice.allowMultiple = true;

    // prepare portrait
    this.portrait = game.add.sprite(this.portraitX,this.portraitY);
    this.portrait.scale.setTo(.7,.7);
    this.portrait.scale.x *= -1;
    this.portrait.fixedToCamera = true;

    // offsets for the real bars
    this.barOffset   = 60;

    // prepare HP bar
    this.hpBarWidth  = 150;
    this.hpBarHeight = 15;

    this.maxHpBar = this.portrait.addChild(game.make.graphics(0, this.order%2 ? 30 : -25));

    this.maxHpBar.lineStyle(2, 0x292929);
    this.maxHpBar.beginFill(0x292929, .6);
    this.maxHpBar.drawPolygon([0,0,-1*this.hpBarWidth+20,0,-1*this.hpBarWidth,-1*this.hpBarHeight,0,-1*this.hpBarHeight,0,0]);

    this.hitHpBar = this.maxHpBar.addChild(game.make.graphics(2, -1));

    this.hitHpBar.beginFill(0xff0000);
    this.hitHpBar.drawPolygon([0,0,-1*this.hpBarWidth+20,0,-1*this.hpBarWidth,-1*this.hpBarHeight+2,0,-1*this.hpBarHeight+2,0,0]);

    this.realHpBar = this.maxHpBar.addChild(game.make.graphics(2, -1));

    this.realHpBar.beginFill(0x00ff00);
    this.realHpBar.drawPolygon([0,0,-1*this.hpBarWidth+20,0,-1*this.hpBarWidth,-1*this.hpBarHeight+2,0,-1*this.hpBarHeight+2,0,0]);

    // prepare EX bar
    this.exBarWidth  = this.hpBarWidth-40;

    this.maxExBar = this.maxHpBar.addChild(game.make.graphics(0, 0));

    this.maxExBar.lineStyle(3, 0x292929);
    this.maxExBar.beginFill(0xebebeb, .6);
    this.maxExBar.drawPolygon([0,0,-1*this.exBarWidth,0,-1*this.exBarWidth+20,this.hpBarHeight/2,0,this.hpBarHeight/2,0,0]);

    this.realExBar = this.maxExBar.addChild(game.make.graphics(2, 1));

    this.realExBar.beginFill(0xffff00);
    this.realExBar.drawPolygon([0,0,-1*this.exBarWidth,0,-1*this.exBarWidth+20,this.hpBarHeight/2-2,0,this.hpBarHeight/2-2,0,0]);

    this.realExBar.width = 0;

    // portraits
    this.portrait.parts = {
        ring : this.portrait.addChild(game.make.sprite(0,20,this.id+'_ring')),
        face : this.portrait.addChild(game.make.sprite(0,0, this.id+'_face')),
        eyes : this.portrait.addChild(game.make.sprite(-64,-64,this.id+'_eyes'))
    };

    this.portrait.parts.ring.anchor.setTo(.5,.5);
    this.portrait.parts.ring.scale.setTo(.8,.8);
    this.portrait.parts.ring.angle = 15;

    game.add.tween(this.portrait.parts.ring.scale).to( { x: 1, y: 1 }, 800, Phaser.Easing.Default, true, 0, -1, true);

    this.portrait.parts.face.anchor.setTo(.5,.5);

    this.portrait.parts.eyes.alpha = 0;

    var eyesOpen  = game.add.tween(this.portrait.parts.eyes).to( {alpha: 0}, 100, Phaser.Easing.Default, false, 100);
    var eyesClose = game.add.tween(this.portrait.parts.eyes).to( {alpha: 1}, 100, Phaser.Easing.Default, true, 2000);

    eyesClose.onComplete.add(function(){
        eyesOpen.start();
    });

    eyesOpen.onComplete.add(function(){
        eyesClose.start();
    });

    // prepare animations
    this.animations = [];
    var atlas = game.cache.getJSON('JSON_animations')[this.id];
    for(var a in atlas){
        this.animations[a] = this.sprite.animations.add(
            a, Phaser.Animation.generateFrameNames(
                atlas[a].prefix,
                atlas[a].frame_start,
                atlas[a].frame_end,
                atlas[a].suffix,
                2
            ), atlas[a].speed, atlas[a].loop
        );
    }

    // prepare animation chains
    this.animChains = game.cache.getJSON('JSON_animations_chains')[this.id];

    // prepare tweens
    this.tweens = {
        jumpIn      : game.add.tween(this.sprite).to({x: this.x, y: this.y}, 400, "Sine.easeInOut"),
        jumpOut     : game.add.tween(this.sprite).to({x: this.actorsPos[this.order].x, y: this.actorsPos[this.order].y}, 1000, "Sine.easeInOut"),
        //specialIn 	: game.add.tween(this.specialBg).to({alpha:1}, 500, "Linear"),
        //specialOut  : game.add.tween(this.specialBg).to({alpha:0}, 1000, "Linear", false, 500),
        cutIn 		: game.add.tween(this.cutIn).to({alpha:1}, 100, "Linear"),
        cutOut 	    : game.add.tween(this.cutIn).to({alpha:0}, 300, "Linear"),
        hurt1       : game.add.tween(this.sprite).to({angle: -10, x: this.actorsPos[this.order].x-50}, 500, Phaser.Easing.Cubic.Out, false, 0, 0, true),
        hurt2       : game.add.tween(this.sprite).to({y: this.actorsPos[this.order].y-10}, 50, "Linear", false, 0, 2, true),
        wait 	    : game.add.tween(this.sprite).to({}, 2000, "Linear")
    };

    this.isEnGarde = function(){
        return this.guard;
    };

    this.isAlive = function(){
        return this.alive;
    };

    this.getEx = function(){
        return this.ex;
    };

    // remove hp
    this.removeHp = function(val, element){

        // if actor is defending reduce damage + no affinity check
        if(this.guard){
            val /= 3;
            this.addEx(5);
        }
        else{
            this.addEx(20);

            // check type affinity
            if(~this.affinity.weak.indexOf(element)){
                val *= 3;
            } else if(~this.affinity.strong.indexOf(element)){
                val /= 3;
            }
        }

        // float to int for better display
        val = Math.floor(val);
        this.hp -= val;

        if(this.hp < 0) this.hp = 0;
        if(this.hp > this.maxHp) this.hp = this.maxHp;

        var offset = this.hp ? this.barOffset : 0;
        var percentHp  = 100*this.hp/this.maxHp;
        var percentBar = (this.hpBarWidth-this.barOffset)/100;

        game.add.tween(this.realHpBar).to({width:offset+percentBar*percentHp}, 300, "Linear", true);
        game.add.tween(this.hitHpBar).to({width:offset+percentBar*percentHp}, 2000, "Linear", true);

        if(!this.guard && val>0){
            this.tweens.hurt1.start();
        }
        this.tweens.hurt2.start();

        // damage pop up
        var colorByType = '#fff';
        switch (element){ // version future : faire un json des types ! ;)
            case 'ice'      : colorByType = '#0000ff'; break;
            case 'wind'     : colorByType = '#00ff00'; break;
            case 'fire'     : colorByType = '#ff0000'; break;
            case 'thunder'  : colorByType = '#ffff00'; break;
        }

        var dmgText = game.add.text(this.sprite.x,this.sprite.y-this.sprite.height/2,'-'+val,{font: '50px Arial', fill: colorByType});
        dmgText.anchor.setTo(.5,.5);
        dmgText.stroke = "#000000";
        dmgText.strokeThickness = 2;

        var dmgTween = game.add.tween(dmgText).to({y:'-100', alpha:0}, 2000, "Linear");
        dmgTween.onComplete.addOnce(function(){
            dmgText.destroy();
        });
        dmgTween.start();

        if(this.hp <= 0){
            this.alive = false;
            this.die();
        }
    };

    this.die = function(){
        this.play('death');
    };

    // add ex gauge
    this.addEx = function(val){
        this.ex += val;

        if(this.ex < 0) this.ex = 0;
        if(this.ex > 100) this.ex = 100;

        var offset = this.ex ? this.barOffset : 0;
        var percentBar = (this.exBarWidth-this.barOffset)/100;

        game.add.tween(this.realExBar).to({width:offset+percentBar*this.ex}, 300, "Linear", true);
    };

    // battle intro
    this.enterScene = function(){
        this.play('run');
        setTimeout(function(that){
            return function(){
                that.say('intro');
            };
        }(this), 1000*this.order);

        var runIn = game.add.tween(this.sprite).to({x:this.actorsPos[this.order].x}, 2000, Phaser.Easing.Quartic.InOut, false, this.order*300);

        runIn.onComplete.add(function(that){
            return function(){
                that.play('idle');
            };
        }(this));

        runIn.start();
    };

    // play animation
    this.play = function(anim, speed, loop){
        this.sprite.play(anim, speed, loop);
    };

    // play voice
    this.say = function(voice){
        this.voice.play(voice);
    };

    // defend
    this.defend = function(){
        this.guard = true;
        this.play('block');
    };

    // stop defending
    this.stopDefend = function(){
        this.guard = false;
        this.play('idle');
    };

    // stop defending
    this.winPose = function(){
        this.action('victory', false, function(that){
            return function(){
                that.play('victory_idle', 10, true);
            };
        }(this))
    };

    // attack an enemy
    this.attack = function(target, cb){
        this.addEx(10);
        if(target.isFlying())
            this.action('attack_0', target, cb); // ground attack
        else
            this.action('attack_1', target, cb); // high attack
    };

    // special attack an enemy
    this.special = function(target, cb){
        this.addEx(-100);
        this.cutIn.bringToTop();
        this.action('special', target, cb);
    };

    // launch action
    this.action = function(key, target, cb){
        if(target){
            var targetXY = target.getXY();
            targetXY.x += this.sprite.width/2;

            this.tweens.jumpIn = game.add.tween(this.sprite).to(targetXY, 400, "Sine.easeInOut");
        }

        var list  = this.animChains[key];
        var actor = this;

        actor.animations[list[list.length-1].animation].onComplete.addOnce(function(){
            actor.play('idle');
            cb();
        });

        var playAction = function(action){
            return function(){
                actor.play(action.animation, action.speed, action.loop);

                if(action.animation == 'special_4_idle' || action.animation == 'special_5_idle'){

                    // 5 non elemental hits
                    for(i=0;i<5;i++)
                        target.removeHp(game.rnd.integerInRange(200, 400), 'special', i*300);
                }

                if(target && action.hit){
                    // damage calculation = str +/- 10%
                    var dmg = Math.floor(actor.strength + actor.strength*game.rnd.integerInRange(0, 20)/100 - actor.strength/10);
                    target.removeHp(dmg, actor.type, action.hit);
                }

                if(action.voice)
                    actor.say(action.voice);

                if(action.tween)
                    actor.tweens[action.tween].start();
            }
        };

        for(var i=list.length-1;i>0;i--){
            if(list[i-1].tween){
                actor.tweens[list[i-1].tween].onComplete.addOnce(playAction(list[i]));
            } else {
                actor.animations[list[i-1].animation].onComplete.addOnce(playAction(list[i]));
            }
        }

        actor.play(list[0].animation, list[0].speed, list[0].loop);

        if(list[0].voice)
            actor.say(list[0].voice);

        if(list[0].tween)
            actor.tweens[list[0].tween].start();
    }
}