var game = new Phaser.Game(800, 600, Phaser.AUTO, 'tests');

game.state.add('Boot', Boot);
game.state.add('Preloader', Preloader);
game.state.add('Map', Map);
game.state.add('Title', Title);
game.state.add('Tuto', Tuto);
game.state.add('Battle', Battle);
game.state.add('End', End);

game.state.start('Boot');