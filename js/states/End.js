var INPUT_LOCK = true;

var End = {

    create: function(){
        INPUT_LOCK = true;
        game.world.setBounds(0, 0, 800, 600);

        var bgm = game.add.audio('bgm_title');
        bgm.loop = true;
        bgm.play();

        // texts
        var intro1 = game.add.text(50,100,'After the defeat of its guardian, the tower collapsed and fell to pieces.', {font:'16px Arial', fill: '#fff'});
        intro1.alpha = 0;

        var intro2 = game.add.text(50,200,'Miraculously, neither Labrys nor her new friends were hurt.', {font:'16px Arial', fill: '#fff'});
        intro2.alpha = 0;

        var intro3 = game.add.text(50,300,'They each went their ways, and soon enough was the strange tower forgotten.\nHowever, they have a feeling they will meet again sometime...', {font:'16px Arial', fill: '#fff'});
        intro3.alpha = 0;

        var intro4 = game.add.text(50,400,'THE END', {font:'16px Arial', fill: '#fff'});
        intro4.alpha = 0;

        // call to action
        var cta = game.add.text(game.world.centerX,500,'Press ENTER to go back', {font:'16px Arial', fill: '#fff'});
        cta.anchor.setTo(.5,.5);
        cta.alpha = 0;

        game.add.tween(intro1).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 0);
        game.add.tween(intro2).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 4000);
        game.add.tween(intro3).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 8000);
        game.add.tween(intro4).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 12000);

        var ctaTween = game.add.tween(cta).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 12000, -1, true);
        ctaTween.onLoop.addOnce(function(){
           INPUT_LOCK = false;

            enterkey.onDown.addOnce(start);
        });

        var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        var start =  function(){
            if(INPUT_LOCK) return;

            bgm.stop();
            game.state.start('Map');
        };
    }
};