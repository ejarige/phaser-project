var INPUT_LOCK = true;

var Tuto = {

    create: function(){
        if(localStorage.getItem("stageClear"))
            game.state.start('Map');
        else
        localStorage.setItem("stageClear", 0);

        INPUT_LOCK = true;

        // texts
        var intro1 = game.add.text(50,100,'This game is an RPG.\nAdvance through the story and unlock data on some myths, gods and deities.', {font:'16px Arial', fill: '#fff'});
        intro1.alpha = 0;

        var intro2 = game.add.text(50,200,'Controls:\nZ = Up ; S = Down ; ENTER = Confirm ; Q = Cancel', {font:'16px Arial', fill: '#fff'});
        intro2.alpha = 0;

        var intro3 = game.add.text(50,300,'In this game, attacks have elements. Pay attention to the numbers and colours to understand the\nrelations between them and take advantage of it.', {font:'16px Arial', fill: '#fff'});
        intro3.alpha = 0;

        var intro4 = game.add.text(50,400,'Your progress will be auto-saved after each battle.\nNow if you are ready...', {font:'16px Arial', fill: '#fff'});
        intro4.alpha = 0;

        // call to action
        var cta = game.add.text(game.world.centerX,500,'Press ENTER to start', {font:'16px Arial', fill: '#fff'});
        cta.anchor.setTo(.5,.5);
        cta.alpha = 0;

        game.add.tween(intro1).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 0);
        game.add.tween(intro2).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 4000);
        game.add.tween(intro3).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 8000);
        game.add.tween(intro4).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 12000);

        var ctaTween = game.add.tween(cta).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 12000, -1, true);
        ctaTween.onLoop.addOnce(function(){
           INPUT_LOCK = false;

            enterkey.onDown.addOnce(start);
        });

        var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        var start =  function(){
            if(INPUT_LOCK) return;

            game.state.start('Map');
        };
    }
};