var Preloader = {

    preload: function(){
        var stages  = game.cache.getJSON('JSON_stages');
        var actors  = game.cache.getJSON('JSON_actors');
        var enemies = game.cache.getJSON('JSON_enemies');

        // title screen
        game.load.image('logo_title', 'assets/img/title_logo.png');
        game.load.audio('bgm_title', 'assets/audio/bgm/title.ogg');
        game.load.audio('bgm_win', 'assets/audio/bgm/win.ogg');
        game.load.video('bg_title', 'assets/img/backgrounds/title.mp4');

        // map
        game.load.image('bg_map', 'assets/img/backgrounds/bg000.png');
        game.load.image('bg_map_tower', 'assets/img/backgrounds/bg001.png');
        for(var i=0;i<6;i++)
            game.load.audio('bgm_map_'+i, 'assets/audio/bgm/m00'+i+'.ogg');

        // stages
        for(var s in stages){
            if(!game.cache.checkKey(Phaser.Cache.IMAGE, stages[s].bg))
                game.load.image(stages[s].bg, 'assets/img/backgrounds/' + stages[s].bg);

            if(!game.cache.checkKey(Phaser.Cache.IMAGE, stages[s].bgm))
                game.load.audio(stages[s].bgm, 'assets/audio/bgm/' + stages[s].bgm);
        }

        // battle
        game.load.image('target', 'assets/sprites/battle/target.png');

        //characters
        for(var p in actors){
            game.load.image(p+'_face', 'assets/sprites/face/' + p + '/face.png');
            game.load.image(p+'_eyes', 'assets/sprites/face/' + p + '/eyes.png');
            game.load.image(p+'_ring', 'assets/sprites/face/' + p + '/effect.png');
            game.load.image(p+'_cut_in', 'assets/img/cut_in/' + p + '.png');

            //game.load.json(p+'_animations', 'data/characters/'+p +'/atlas_animations.json');

            game.load.atlas(p+'_sprite', 'assets/sprites/spritesheets/' + p + '.png', 'data/characters/' + p + '/atlas_sprites.json');
            game.load.audiosprite(p+'_voice', 'assets/audio/voices/' + p + '.ogg', 'data/characters/' + p + '/atlas_voices.json');
        }

        // monsters
        for(var m in enemies)
            game.load.image(enemies[m].sprite, 'assets/sprites/demons/'+enemies[m].sprite);

        var load = game.add.text(50,400,'LOADING...', {font:'16px Arial', fill: '#fff'});
    },

    create: function(){
        game.state.start('Title', true, false);
    }
};