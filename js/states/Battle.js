var VICTORY = false;
var INPUT_LOCK = true;

var Battle = {

    upKey    : void 0,
    downKey  : void 0,
    validKey : void 0,
    backKey  : void 0,

    SELECT_TARGET : false,
    selectedTarget: 0,

    bgm     : void 0,
    stage   : void 0,
    stageId : 0,
    actors  : [],
    enemies : [],

    enemiesKilled : 0,
    actorsKilled  : 0,

    turnOrder : [],
    activeTurn : 0,
    totalTurns : 1,

    activeActor : void 0,
    actionMessage : void 0,

    actions: [
        {
            name: "Attack",
            description: "Basic elemental attack on one enemy",
            onSelect: function(that){
                that.attack();
            },
            onTargetSelect: function(that){
                INPUT_LOCK = true;
                that.unselectAction();

                var nextTurn = function(that){
                    return function(){
                        that.endTurn(that);
                    };
                }(that);
                that.actors[that.activeActor].attack(that.enemies[that.selectedTarget], nextTurn);
            }
        },
        {
            name: "Special",
            description: 'Non-elemental damage to one enemy',
            onSelect: function(that){
                that.special();
            },
            onTargetSelect: function(that){
                INPUT_LOCK = true;
                that.unselectAction();

                var nextTurn = function(that){
                    return function(){
                        that.endTurn(that);
                    };
                }(that);
                that.actors[that.activeActor].special(that.enemies[that.selectedTarget], nextTurn);
            }
        },
        {
            name: "Guard",
            description: 'Cover your weak spots and defend for one turn, reducing damage',
            onSelect: function(that){
                that.defend();
            }
        },
        {
            name:'Run',
            description: 'Flee from battle and return to stage selection',
            onSelect: function(that){
                that.run();
            }
        },
        /*{
            name:'Debug',
            description: 'Command used for debugging',
            onSelect: function(that){
                that.debug();
            }
        },*/

    ],
    selectedAction: 0,

    init: function(stageId){
        this.stageId = stageId;
    },

    create: function(){
        INPUT_LOCK = true;
        VICTORY = false;
        this.stage = game.cache.getJSON('JSON_stages')[this.stageId];

        game.world.setBounds(0, 0, 1071, 600);

        this.upKey      = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        this.downKey    = game.input.keyboard.addKey(Phaser.Keyboard.S);
        this.validKey   = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.backKey    = game.input.keyboard.addKey(Phaser.Keyboard.Q);

        var bg = game.add.sprite(game.world.width/2,game.world.height/2, this.stage.bg);
        bg.anchor.setTo(.5,.5);

        this.bgm = game.add.audio(this.stage.bgm);
        this.bgm.loop = true;
        this.bgm.volume = .8;
        this.bgm.play();

        // enemies
        var foes      = game.cache.getJSON('JSON_enemies');
        var foesGroup = game.cache.getJSON('JSON_enemy_groups')[this.stage.enemy_group];

        for(var e in foesGroup){
            var foe = new Enemy(foes[foesGroup[e].enemy]);
            foe.setXY(foesGroup[e].x,foesGroup[e].y);

            this.enemies.push(foe);
        }

        // actors
        for(var p in this.stage.party){
            var actor = new Actor(this.stage.party[p], p);

            actor.play('idle');
            this.actors.push(actor);
        }

        this.actors[0].sprite.bringToTop();
        if(this.actors.length > 3)
            this.actors[3].sprite.bringToTop();


        // UI - Current turn
        this.currentTurnText = game.add.text(780,20, 'Current turn :', {font: '14px Arial', fill: '#f57c00'});
        this.currentTurnText.anchor.setTo(1,0);
        this.currentTurnText.fontWeight = 'bold';
        this.currentTurnText.stroke = '#000000';
        this.currentTurnText.strokeThickness = 4;
        this.currentTurnText.fixedToCamera = true;


        this.currentTurn = game.add.text(780,40, '', {font: '24px Arial', fill: '#FFFFFF'});
        this.currentTurn.anchor.setTo(1,0);
        this.currentTurn.stroke = '#000000';
        this.currentTurn.strokeThickness = 4;
        this.currentTurn.fixedToCamera = true;

        // UI - Message window
        this.messageWindow = game.add.graphics(0,game.world.height);
        this.messageWindow.beginFill(0, .6);
        this.messageWindow.drawRect(0,0,game.world.width, game.world.height-550);

        this.actionMessage = game.add.text(game.world.centerX, 578, '', {font:'16px Arial', fill:'#fff'});
        this.actionMessage.anchor.setTo(.5,.5);
        this.actionMessage.alpha = 0;

        // UI - Actions Menu
        var i = 0;
        for(var a in this.actions){
            this.actions[a].button = game.add.text(20,150+i*24,this.actions[a].name, {font: '20px Arial', fill: '#FFFFFF'});
            this.actions[a].button.fixedToCamera = true;
            this.actions[a].button.stroke = '#000000';
            this.actions[a].button.strokeThickness = 4;
            i++;
        }

        // UI - Victory
        this.victoryMonster = foes[this.stage.victory_msg];

        this.victoryMsgBox = game.add.graphics(game.world.width+1,0);
        this.victoryMsgBox.beginFill(0, .8);
        this.victoryMsgBox.drawRect(0,0,game.world.width/2,game.world.height);

        this.victoryTitle = game.add.text(game.world.width+1, 50, 'VICTORY !', {font: '32px Arial', fill: '#F57C00'});
        this.victoryDyk   = game.add.text(game.world.centerX+50, 114, 'Did you know ?', {font: '16px Arial', fill: '#FFFFFF'});
        this.victoryDyk.alpha = 0;

        this.victoryImg   = game.add.sprite(game.world.centerX+50, 146, this.victoryMonster.sprite);
        this.victoryImg.scale.setTo(.5,.5);
        this.victoryImg.alpha = 0;

        this.victoryMsg   = game.add.text(game.world.centerX+50, 146+this.victoryImg.height+16, this.victoryMonster.description, {font: '14px Arial', fill: '#FFFFFF', wordWrap: true, wordWrapWidth: 320});
        this.victoryMsg.alpha = 0;

        this.vitoryBack = game.add.text(game.world.centerX+50, 550, "Press ENTER to return to stage selection",{font: '16px Arial', fill: '#FFFFFF'});
        this.vitoryBack.alpha = 0;

        this.victoryTween = function(){
            game.add.tween(this.victoryMsgBox).to({x:game.world.centerX}, 300, Phaser.Easing.Cubic.Out, true);
            game.add.tween(this.victoryTitle).to({x:game.world.centerX+50}, 300, Phaser.Easing.Elastic.Out, true, 300);
            game.add.tween(this.victoryDyk).to({alpha:1}, 2000, "Linear", true, 1000);
            game.add.tween(this.victoryImg).to({alpha:1}, 2000, "Linear", true, 1500);
            game.add.tween(this.victoryMsg).to({alpha:1}, 2000, "Linear", true, 2000);
            game.add.tween(this.vitoryBack).to({alpha:1}, 1000, "Linear", true, 5000, -1, true);
            setTimeout(function(){
                VICTORY = true;
            }, 5000);
        };

        // cta
        this.upKey.onDown.add(this.upAction,this);
        this.downKey.onDown.add(this.downAction,this);
        this.backKey.onDown.add(this.backAction,this);
        this.validKey.onDown.add(this.validAction,this);

        this.startBattle();
    },

    upAction: function(){
        if(this.SELECT_TARGET){
            if(INPUT_LOCK) return;

            this.selectTarget(this.selectedTarget-1);
        } else {
            this.selectAction(this.selectedAction-1);
        }
    },

    downAction: function(){
        if(INPUT_LOCK) return;

        if(this.SELECT_TARGET){
            this.selectTarget(this.selectedTarget+1);
        } else {
            this.selectAction(this.selectedAction+1);
        }
    },

    backAction: function(){
        if(INPUT_LOCK) return;

        if(this.SELECT_TARGET){
            this.unselectTarget();
            this.selectAction(this.selectedAction);
            this.SELECT_TARGET = false;
        } else {

        }
    },

    validAction: function(){
        if(INPUT_LOCK && !VICTORY) return;

        if(this.SELECT_TARGET){
            this.actions[this.selectedAction].onTargetSelect(this);
            this.unselectTarget();
        } else if(VICTORY){
            if (+this.stageId == 6){
                this.goToState('End');
            } else {
                this.goToState('Map');
            }
        } else {
            this.actions[this.selectedAction].onSelect(this);
        }
    },

    startBattle: function(){
        this.turnOrder = [];

        for(var a in this.actors){
            this.turnOrder.push({
                id:a,
                type:'actor',
                speed: this.actors[a].speed
            });
        }

        for(var e in this.enemies){
            this.turnOrder.push({
                id:e,
                type:'foe',
                speed: this.enemies[e].speed
            });
        }

        this.turnOrder.sort(function(a,b){
            return a.speed <= b.speed;
        });

        this.activeTurn = 0;

        this.playIntro();
    },

    playIntro: function(){
        for(var a in this.actors)
            this.actors[a].enterScene();

        // UI tweens
        // MsgBox
        var msgboxTween  = game.add.tween(this.messageWindow).to({y:550}, 3000, Phaser.Easing.Default);
        var msgtextTween = game.add.tween(this.actionMessage).to({alpha:1}, 300, Phaser.Easing.Default);

        msgboxTween.onComplete.add(function(){
            msgtextTween.start();
        });

        // camera tweens
        var cameraIntroToRight  = game.add.tween(game.camera).to({x:game.world.width-game.camera.width}, 3000, Phaser.Easing.Default);
        var cameraIntroToLeft   = game.add.tween(game.camera).to({x:game.world.width-game.camera.width-200}, 2000, Phaser.Easing.Default);
        var cameraIntroToCenter = game.add.tween(game.camera).to({x: 136}, 300, Phaser.Easing.Default);

        var onAllComplete =  function(that){
            return function(){
                that.startTurn(0);
                that.selectAction(0);
            };
        }(this);

        cameraIntroToCenter.onComplete.addOnce(onAllComplete);
        cameraIntroToLeft.onComplete.addOnce(function(){
            cameraIntroToCenter.start();
        });
        cameraIntroToRight.onComplete.addOnce(function(){
            cameraIntroToLeft.start();
        });

        cameraIntroToRight.start();
        msgboxTween.start();
    },

    startTurn: function(t){
        if(t>=this.turnOrder.length){
            t = 0;
            this.totalTurns++;
        }
        var currentActor = this.turnOrder[t];
        this.activeTurn = t;

        var that = this;
        var nextTurn = function(that){
            return function(){
                that.endTurn(that);
            };
        }(that);

        if(currentActor.type == 'actor'){
            INPUT_LOCK = false;
            this.activeActor = currentActor.id;
            this.currentTurn.setText(this.actors[this.activeActor].name);

            this.actors[this.activeActor].say('turn_start');

            if(this.actors[this.activeActor].isEnGarde())
                this.actors[this.activeActor].stopDefend();

            this.selectAction(this.selectedAction);
        } else if (currentActor.type == 'foe'){
            this.currentTurn.setText(this.enemies[currentActor.id].getName());

            // check if that target is alive, if not try again
            do{
                var randTarget = game.rnd.integerInRange(0, this.actors.length-1);
            } while(!this.actors[randTarget].isAlive());

            this.enemies[currentActor.id].attack(this.actors, randTarget, this.totalTurns, nextTurn);
        }

    },

    endTurn: function(that){
        for(var i in that.enemies)
            if(!that.enemies[i].isAlive())
                for(var o in that.turnOrder)
                    if(that.turnOrder[o].type == 'foe' && that.turnOrder[o].id == i){
                        that.turnOrder.splice(o,1);
                        that.enemiesKilled++;
                    }


        for(var i in that.actors)
            if(!that.actors[i].isAlive())
                for(var o in that.turnOrder)
                    if(that.turnOrder[o].type == 'actor' && that.turnOrder[o].id == i){
                        that.turnOrder.splice(o,1);
                        that.actorsKilled++;
                    }

        if(that.actorsKilled == that.actors.length){
            that.defeat();
        } else if(that.enemiesKilled == that.enemies.length){
            that.victory();
        } else {
            that.startTurn(that.activeTurn+1);
        }
    },

    victory: function(){
        this.bgm.stop();
        this.bgm = game.add.audio('bgm_win');
        this.bgm.loop = true;
        this.bgm.play();

        for(var a in this.actors)
            if(this.actors[a].isAlive())
                this.actors[a].winPose();

        this.victoryTween();

        var save = localStorage.getItem("stageClear");
        if(+save <= +this.stageId && +this.stageId < 6){
            localStorage.setItem("stageClear", +this.stageId+1);
        }
    },

    defeat: function(){
        this.goToState('Map');
    },

    setButtonColor: function(i, color){
         this.actions[i].button.setStyle({font: '20px Arial', fill:color});

         this.actions[i].button.stroke = '#000000';
         this.actions[i].button.strokeThickness = 4;
    },

    selectAction: function(s){
        if(s < 0)
            s = this.actions.length-1;
        else if(s > this.actions.length-1)
            s = 0;

        for(var i in this.actions)
            if(i == s)
                this.setButtonColor(i, 'orange');
            else
                this.setButtonColor(i, 'white');

        this.actionMessage.setText(this.actions[s].description);

        this.selectedAction = s;
    },

    unselectAction: function(){
        this.actionMessage.setText('');
        for(var i in this.actions)
            this.setButtonColor(i, 'white');

        this.selectedAction = 0;

    },

    selectTarget: function(s){
        if(s < 0)
            s=this.enemies.length-1;
        else if(s > this.enemies.length-1)
            s=0;

        var up = s > this.selectedTarget;
        while(!this.enemies[s].isAlive()){
            if(up)
                s++;
            else
                s--;

            if(s < 0)
                s=this.enemies.length-1;
            else if(s > this.enemies.length-1)
                s=0;
        }

        for(var i in this.enemies)
                if(i == s)
                    this.enemies[i].toggleSelect(true);
                else
                    this.enemies[i].toggleSelect(false);

        this.actionMessage.setText(this.enemies[s].getInfo());

        this.selectedTarget = s;
    },

    unselectTarget: function(){
        this.SELECT_TARGET = false;
        this.actionMessage.setText('');

        for(var i in this.enemies)
            this.enemies[i].toggleSelect(false);
    },

    goToState: function(state){
        this.bgm.fadeOut(2000);

        game.camera.fade('#000000', 2000);
        game.camera.onFadeComplete.add(function(){
            this.bgm.stop();

            this.activeTurn    = 0;
            this.totalTurns    = 1;
            this.actorsKilled  = 0;
            this.enemiesKilled = 0;

            this.actors = [];
            this.enemies = [];
            this.turnOrder = [];

            game.state.start(state);
        },this);
    },

    attack: function(){
        /*for(var a in this.actors){
            var randEnemy = game.rnd.integerInRange(0, this.enemies.length-1);
            this.actors[a].special(this.enemies[randEnemy]);
        }*/
        this.SELECT_TARGET = true;
        this.selectedTarget = 0;
        this.selectTarget(this.selectedTarget);
    },

    special: function(){
        var ex = this.actors[this.activeActor].getEx();
        if(ex < 100){
            this.actionMessage.setText('Ex Gauge needs to be full (current : '+ex+'%)');
            return;
        }

        this.SELECT_TARGET = true;
        this.selectedTarget = 0;
        this.selectTarget(this.selectedTarget);
    },

    defend: function(){
        INPUT_LOCK = true;
        this.unselectAction();

        this.actors[this.activeActor].defend();
        this.endTurn(this);
    },

    run: function(){
        INPUT_LOCK = true;

        this.goToState('Map');
    }
};