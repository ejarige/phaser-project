var INPUT_LOCK = true;

var Title = {

    create: function(){
        INPUT_LOCK = true;

        // fade in
        game.camera.flash('#000000');

        // background
        var video = game.add.video('bg_title');
        var bg    = video.addToWorld(game.world.centerX, game.world.centerY, 0.5, 0.5);

        bg.width = 800;
        video.play(true);

        // music
        var bgm = game.add.audio('bgm_title');
        bgm.volume = .6;
        bgm.loop = true;
        bgm.play();

        // blue filter
        var filter = game.add.graphics(0,0);
        filter.beginFill(0x00BBFA, 0.5);
        filter.drawRect(0, 120, game.world.width, game.world.height-240);
        filter.alpha = 0;

        var logo = game.add.sprite(30,160, 'logo_title');
        logo.scale.setTo(.2,.2);
        logo.alpha = 0;

        // text
        var startText = game.add.text(600,game.world.centerY,'Press ENTER to start', {font:'24px Arial'});
        startText.anchor.setTo(.5,.5);
        startText.alpha = 0;

        game.add.tween(logo).to({alpha:1}, 4000, "Linear", true, 5000);
        game.add.tween(filter).to({alpha:1}, 4000, "Linear", true, 5000);

        var startTween = game.add.tween(startText).to({alpha:1}, 1000, Phaser.Easing.Cubic.Out, true, 10000, -1, true);
        startTween.onLoop.addOnce(function(){
           INPUT_LOCK = false;

            enterkey.onDown.addOnce(start);
        });

        // credits
        var credits = game.cache.getJSON('JSON_credits');
        for(var c in credits){

            var staffTitle = game.add.text(-game.world.width/2,430,credits[c].title, {font:'16px Arial'});
            var staffName  = game.add.text(game.world.width+game.world.width/2,450,credits[c].name, {font:'20px Arial'});

            staffTitle.anchor.setTo(.5,.5);
            staffName.anchor.setTo(.5,.5);

            game.add.tween(staffTitle).to({x:game.world.centerX+20}, 5000, Phaser.Easing.Cubic.Out, true, 10000+c*5000, 0, true);
            game.add.tween(staffName).to({x:game.world.centerX-20}, 5000, Phaser.Easing.Cubic.Out, true, 10000+c*5000, 0, true);
        }

        var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);

        var start =  function(){
            if(INPUT_LOCK) return;

            bgm.fadeOut(3000);
            game.camera.fade('#000000', 3000);
            game.camera.onFadeComplete.add(function(){
                bgm.stop();
                video.stop();
                game.state.start('Tuto');
            },this);
        };
    }
};