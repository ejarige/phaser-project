var Boot = {

    preload: function(){
        // JSON load (some required for preload)
        game.load.json('JSON_actors', 'data/game/actors.json');
        game.load.json('JSON_enemies', 'data/game/enemies.json');
        game.load.json('JSON_enemy_groups', 'data/game/enemy_groups.json');
        game.load.json('JSON_stages', 'data/game/stages.json');
        game.load.json('JSON_animations', 'data/game/animations.json');
        game.load.json('JSON_animations_chains', 'data/game/animation_chains.json');
        game.load.json('JSON_credits', 'data/game/credits.json');
    },

    create: function(){
        game.state.start('Preloader', true, false);
    }
};