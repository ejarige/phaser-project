var INPUT_LOCK = true;
var Map = {
    bg     : void 0,
    tower  : void 0,

    bgm     : [],
    stages  : {},

    saveState       : 0,
    selectedStage   : 0,

    create: function(){
        INPUT_LOCK = true;

        game.world.setBounds(0, 0, 800, 600);
        game.camera.flash('#000000');

        var save = localStorage.getItem("stageClear");
        this.saveState = save ? +save : 0;
        this.selectedStage = save;

        this.bg = game.add.sprite(game.world.centerX,game.world.centerY,'bg_map');
        this.bg.anchor.setTo(.5,.5);
        this.bg.scale.setTo(game.world.width/this.bg.width,game.world.height/this.bg.height);

        this.stageTxt = game.add.text(50, 50, '', {font: '16px Arial', fill: '#FFFFFF', wordWrap: true, wordWrapWidth: 300});

        var stages = game.cache.getJSON('JSON_stages');

        // audio play
        for(var m=0;m<6;m++){
            var bgm = game.add.audio('bgm_map_'+m);
            bgm.loop = true;

            if(m==this.selectedStage || (m==5 && this.selectedStage==6))
                bgm.volume = 1;
            else
                bgm.volume = 0;

            bgm.play();
            this.bgm.push(bgm);
        }

        var i=0;
        for(var s in stages){
            if(s > this.saveState) break;

            var button = game.add.graphics(380+i*50,500-i*60);
            button.beginFill(0x00BBFA, .8);
            button.drawRoundedRect(0, 0, 320, 50, 5);
            button.width = 0;
            button.alpha = .3;

            var text = button.addChild(game.make.text(300,button.height/2+3,'| '+stages[s].name, {font: '20px Arial', fill:'#FFFFFF'}));
            text.anchor.setTo(0,.5);
            text.scale.x *= -1;
            text.alpha = 0;

            game.add.tween(button).to({width:-320}, 1000, Phaser.Easing.Bounce.Out, true, i*700);
            var tweenText = game.add.tween(text).to({alpha: 1}, 1000, Phaser.Easing.Bounce.Out, true, i*700+1000);

            if(i==this.saveState){
                tweenText.onComplete.add(function(that){
                    return function(){
                        that.selectStage(that.selectedStage);
                        INPUT_LOCK = false;
                    }
                }(this))
            }

            stages[s].button = button;
            i++;
        }

        this.tower = game.add.sprite(game.world.centerX,game.world.centerY,'bg_map_tower');
        this.tower.anchor.setTo(.5,.5);
        this.tower.scale.setTo(game.world.width/this.tower.width,game.world.height/this.tower.height);

        var zkey = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        var qkey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        var skey = game.input.keyboard.addKey(Phaser.Keyboard.S);
        var dkey = game.input.keyboard.addKey(Phaser.Keyboard.D);

        zkey.onDown.add(function(){
            if(INPUT_LOCK) return;

            this.selectStage(this.selectedStage+1);
        },this);

        qkey.onDown.add(function(){
            if(INPUT_LOCK) return;

            this.selectStage(0);
        },this);

        skey.onDown.add(function(){
            if(INPUT_LOCK) return;

            this.selectStage(this.selectedStage-1);
        },this);

        dkey.onDown.add(function(){
            if(INPUT_LOCK) return;

            this.selectStage(6);
        },this);

        this.stages = stages;

        var enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enter.onDown.add(this.play,this);
    },

    play: function(){
        if(INPUT_LOCK) return;

        for(var i in this.bgm)
            this.bgm[i].fadeOut(2000);


        game.camera.fade('#000000', 2000);
        game.camera.onFadeComplete.add(function(){
            for(var i in this.bgm)
                this.bgm[i].stop();
            this.bgm = [];
            game.state.start('Battle', true, false, this.selectedStage);
        },this);
    },

    selectStage: function(s){
        if(s < 0)
            s = this.saveState;
        else if(s > this.saveState)
            s = 0;

        for(var i in this.stages){
            if(i > this.saveState) break;
            if(i == s){
                this.stages[i].button.alpha = 1;

                if(i==6)
                    this.bgm[5].fadeTo(2000,1);
                else
                    this.bgm[i].fadeTo(2000,1);
            }
            else{
                this.stages[i].button.alpha = .3;

                if((i<5 && s==6) || (i<6 && s!=6))
                    this.bgm[i].fadeTo(2000,.01);
            }
        }
        this.selectedStage = s;
        this.stageTxt.setText(this.stages[s].description);
    }
};